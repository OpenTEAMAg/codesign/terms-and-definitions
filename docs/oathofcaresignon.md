<h1> Ag Data Oath of Care Sign-on </h1>

<p> Review the Data Fiduciary Oath of Care for Agricultural Professionals on the previous page for alignment with your individual or organization’s work. If you choose to sign on to the Oath of Care and adopt its principles, fill out the below sign on form. Please note, this is not a legally enforceable document, but rather a social contract, setting an ethical precedent that we call on the community to enforce as best practice. </p>
</br>

<iframe src="https://app.surveystack.io/surveys/63d2c6579e2def0001665b14" height="800px" width="100%" title="OpenTEAM Tech Review"></iframe>

<h1> Displaying the Oath of Care </h1> 
<p> After signing on and agreeing to upholding the Oath of Care, it can be represented on your email signature, personal or organizational websites, or social media. </p>

<p> You may use the following messages. You may also choose to include specific language from the Oath of Care document itself: </p>


<div class='tab'>
<i>
 “I have signed on to the Data Fiduciary Oath of Care for Agricultural Professionals” 
</br></br>
“Our organization has signed on to the Data Fiduciary Oath of Care for Agricultural Professionals, agreeing to act in good faith when advising land stewards and collecting data. We agree to prioritizing  the needs and interests of the land and the land steward.” 
</br></br>
“ I (or our organization) has signed on to the Data Fiduciary Oath of Care for Agricultural Professionals, agreeing to act in good faith when advising land stewards and collecting data. I promise to prioritize the needs and interests of the land and the land steward” </i>
</div>

</br>

<h1> Oath of Care Logo </h1>
<p> You may display this logo alongside language stating you have signed on to the oath of care. </p>

<a href="https://gitlab.com/OpenTEAMAg/codesign/terms-and-definitions/-/raw/master/docs/assets/oathofcarev2.png" target="_blank"> Download Here </a> 





