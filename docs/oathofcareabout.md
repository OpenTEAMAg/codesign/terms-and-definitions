# About the Oath of Care

The [Data Fiduciary](/TermsandDefinitions#data_fiduciary) Oath of Care for Agricultural Professionals is a trust-building document between individuals in advisory roles with producers. It is meant to act as a social contract that establishes good-faith activity when it comes to advising and data collection practices, as well as adherence to other OpenTEAM’s Suite of Ag Data Use Agreements. It is intended to be used by technical service and assistance providers and individuals in advisory roles with farmers, ranchers, and [land stewards](/TermsandDefinitions#land_stewards).

Relation to other documents in OpenTEAM’s Ag Data Use Suite of Documents:

- References definitions in the Ag Data Use Glossary
- Establishes best practice for individuals in advisory roles, in order to uphold the principles set out in the Agriculturalists’ Bill of Data Rights
- Serve as a trust-building document that may be shared with a land steward before initiating a Proxy Agreement and accepting Conditional Terms of Use Agreements with a chosen advisor

</br>

# Versioning

*Version 2 Document (Released July 2023)*

We are currently accepting feedback for Version 2.0 of the Oath of Care Starting in 2024, there will be an annual review and versioning process, with the first iterative process taking place quarter one 2024.

Feedback is encouraged at any time. The versioning cycle will be announced with ample time for review in OpenTEAM Working Groups (Hub & Networks, Field Methods, Tech) and in our newsletter.


# Provide Feedback
You may provide feedback or insights on the Oath of Care in the following ways:

1. Visit and contribute to the <a href="https://www.hylo.com/all/post/62891" target="_blank"> discussion on Hylo</a>
2. Provide comments on the linked and embedded Google doc below *(to submit a comment anonymously, log out of your Google account or open the link in a private (incognito) window)*
3. <a href="https://openteam-agreements.community/contact/" target="_blank"> Contact us Directly</a>


## <a href="https://docs.google.com/document/d/1HVhezEh-zY0FH6J_mOCQMuJB-1xInIkEDPopeRnRB6U/edit#heading=h.izvejbdq8lh7" target="_blank"> Provide Comments to the Oath of Care</a>
<iframe src="https://docs.google.com/document/d/1HVhezEh-zY0FH6J_mOCQMuJB-1xInIkEDPopeRnRB6U/edit#heading=h.izvejbdq8lh7" height="800px" width="100%" title="OpenTEAM Tech Review"></iframe>
