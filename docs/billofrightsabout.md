# About the Agriculturalists’ Bill of Data Rights

The Agriculturalists’ Bill of Data Rights is a  set of rights that are ensured to [land stewards](/TermsandDefinitions#land_stewards) in regards to their data within the OpenTEAM technology ecosystem. It is the responsibility of community members within the technology ecosystem to uphold these rights on the behalf of stewards, as well as to implement mechanisms for producer control within their technologies. This document shapes all other OpenTEAM data artifacts, which are meant to maintain producers’ data rights.

This document is to be used by OpenTEAM’s technologist community as a roadmap to maintain producer data rights and by OpenTEAM tech ecosystem users as an educational document to understand their data rights within the ecosystem.

Relation to other documents in OpenTEAM’s Ag Data Use Suite of Documents:

1. References definitions in the Ag Data Use Glossary
2. Establishes foundational rights which all other artifacts are meant to uphold

# Versioning

*Version 1 Document (Released July 2023)*
We are currently accepting feedback for Version 1.0 of the Agriculturalists’ Bill of Data Rights in 2024, there will be an annual review and versioning process, with the first iterative process taking place quarter one 2024.

Feedback is encouraged at any time. The versioning cycle will be announced with ample time for review in OpenTEAM Working Groups (Hub & Networks, Field Methods, Tech) and in our newsletter.


# Provide Feedback
You may provide feedback or insights on the Oath of Care in the following ways:

1. Visit and contribute to the <a href="https://www.hylo.com/all/post/62894" target="_blank"> discussion on Hylo</a>
2. Provide comments on the linked and embedded Google doc below *(to submit a comment anonymously, log out of your Google account or open the link in a private (incognito) window)*
3. <a href="https://openteam-agreements.community/contact/" target="_blank"> Contact us Directly</a>


## <a href="https://docs.google.com/document/d/1TGgQ_yFd0iJel-ksLNUfs4hZQC7aJfBGMjVX5_DGUeI/edit" target="_blank"> Provide Comments to the Agriculturalists’ Bill of Data Rights</a>
<iframe src="https://docs.google.com/document/d/1TGgQ_yFd0iJel-ksLNUfs4hZQC7aJfBGMjVX5_DGUeI/edit" height="800px" width="100%" title="OpenTEAM Tech Review"></iframe>
