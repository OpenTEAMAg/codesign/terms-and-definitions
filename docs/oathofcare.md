<img src="../assets/oathofcarev2.png" alt="Oath of Care" class="imgcenter" width="100" height="100" >
<h1>Preamble</h1>

This oath means to protect the collaborative integrity of [land stewards](/TermsandDefinitions#land_stewards) and their advisors.  It is meant to uphold the intellectual property of [land stewards](/TermsandDefinitions#land_stewards) and support a commitment to the long-term well-being of the land we all rely on.

We believe that farmers, ranchers, and other [land stewards](/TermsandDefinitions#land_stewards) have a right to know that their advisors are acting on their behalf and in good faith. This document is meant to be a tool for a formalized mutual commitment to [data management](/TermsandDefinitions#data_management), and we recommend that [land stewards](/TermsandDefinitions#land_stewards) and advisors maintain a signed copy of this oath when they enter into a partnership. Similarly, we recommend that [land stewards](/TermsandDefinitions#land_stewards) insist the oath be signed by their advisors before entering into a relationship.

Agricultural professionals’ consultation not only influences the health of their clients, but also contributes to generation of common knowledge, public science, and the health of our shared landscape. In recognition of the depth of responsibility bestowed upon individuals in these roles, we present a set of principles to which we hold each other [accountable](/TermsandDefinitions#accountable). 

In its original form, the [Hippocratic Oath](/TermsandDefinitions#hippocratic_oath) requires a new physician to swear to uphold specific ethical standards. Of historic and traditional value, the oath is considered a rite of passage for practitioners of medicine. But why only physicians? Why not holders of public data, why not engineers, why not scientists, why not businesses, and why not agriculturalists? What about the care of the data about the health and well-being of our public atmosphere, our soils, our water, and our data about how the system of our biosphere functions and how we can create health or destroy it? It seems time that we extend to the agricultural profession a version of the physician’s and the fiduciary oath and to treat [land stewards](/TermsandDefinitions#land_stewards) and the data about the land with the same care and confidence that a doctor would their patient.* 

</br>
<h1 style="text-align: center;">Data Fiduciary Oath of Care for Agricultural Professionals</h1>


<p style="text-align: center;"><strong>Putting the Land Steward’s interests first</strong></p>

I shall put the land and [land steward](/TermsandDefinitions#land_stewards)'s needs and interests ahead of my own. Therefore, I am proud to commit to the following five fiduciary principles:

 
  1. I will always put the land steward’s best interests first, and help, to the best of my ability, to navigate short and long-term tradeoffs.
  2. I will act with prudence; that is, with the skill, care, diligence, and sound judgment of a professional.
  3. I will not mislead. I will provide the whole story and make transparent, full, and fair disclosure of all important facts.
  4. I will avoid conflicts of interest, and should such situations arise, I will immediately disclose the nature of the conflict.
  5. If unavoidable conflicts occur, I will fairly manage them, in the land steward's favor. 
  6. I agree to uphold the <a href="https://openteam-agreements.community/billofrights/" target="_blank"> Agriculturalists’ Bill of Data Rights</a> to the best of my ability in all my work.  


Furthermore, I swear to those in my chosen profession to fulfill, to the best of my ability and judgment, this covenant: 


  1. I will respect the hard-won scientific gains of those agricultural professionals in whose steps I walk, and gladly share such knowledge as is mine with those who are to follow.
  2. I will apply all measures that are required to not sacrifice long-term outcomes for short-term results.
  3. I will remember that there is an art to agriculture as well as science, and that planning, observation, analysis, and collaboration with others may be the first path before taking action.  
  4. I will not give the people who use my recommendations false comfort about their accuracy. Instead, I will make explicit my assumptions and potential oversights.    I will not be ashamed to say “I don’t know” nor will I fail to call in my colleagues when the skills of another are needed. 
  5. I understand that my work may have enormous effects on society and the economy, many of them beyond my comprehension, and I look for equitable outcomes and accessible processes for the well-being of humanity and land.
  6. I will respect the privacy of [land stewards](/TermsandDefinitions#land_stewards) and their operations, for their personal challenges are shared in confidence.  
  7. I will remember that I remain a member of society, with obligations to all my fellow human beings to improve the most basic regenerative systems that support us all—those in cities and towns, and those in the country, and those of differing abilities.
  8. If and when I find myself unable to carry out these duties, I agree to pass along my accumulated knowledge to those who are willing and eager to uphold this oath.

<br>

<i>*Adapted from Dr. Louis Lasagna’s 1964 Hippocratic Oath and the Fiduciary Oath used by financial advisors.
**Inspired by the Land Steward’s Hippocratic Oath </i>
<br>

<!-- [Sign on to the Oath of Care](~/docs/OathSignon.md){ .md-button .md-button--primary .center }  -->
<br>
<div class ="center">

 <button class="btn btn-success md-button md-button--primary center onclick=" onclick=" window.open('https://openteam-agreements.community/oathofcaresignon/','_blank')"> Sign on to the Oath of Care</button>



</div>




