<!-- ---
hide:
  - path
--- -->
<style>
.icon-container {
  display: flex;
  align-items: center;
}

.icon-image {
  height: 100px;
  margin-right: 10px;
}

h3 {
  align-items: center;
  display: flex;
}


</style>

<!--
<p>Click on a letter to jump to the corresponding section in the glossary:</p>
[A](#a) | [B](#b) | [C](#c) | [D](#d) | [E](#e) | [F](#f) | [G](#g) | [H](#h) | [I](#i) | [J](#j) | [K](#k) | [L](#l) | [M](#m) | [N](#n) | [O](#o) | [P](#p) | [Q](#q) | [R](#r) | [S](#s) | [T](#t) | [U](#u) | [V](#v) | [W](#w) | [X](#x) | [Y](#y) | [Z](#z)
-->

<img src="../assets/glossaryv1.png" alt="Glossary Icon" class="imgcenter" width="100" height="100" >

<p>The purpose of the Ag Data Glossary is to establish a shared language and understanding of agricultural data use concepts that are used throughout  OpenTEAM’s Suite of Ag Data Use Agreements. It serves as a resource for technologists, technical assistance providers, land stewards, and any other users of OpenTEAM’s Suite of Ag Data Use Agreements. Key terms and definitions within the Ag Data Use Glossary are hyperlinked in all other Ag Data Use Agreement documents.</p> 

<p> These terms and definitions have been developed to build community alignment for terminology within these documents, and do not necessarily reflect definitions of terms outside of this context. </p>

<p>These definitions will be updated as our societal understanding of agricultural data use and all of the embedded nuances evolves. We rely on our community of technologists, land stewards, policy-makers, academics, and thought leaders to help us version and maintain these definitions. We welcome feedback and contribution from all interested individuals or groups to keep this resource accurate and current.</p>

<p> Foundational policies and principles that we define for our work draw from the following references: General Data Protection Regulation (GDPR), Australian Farm Data Code, CARE Principles for Indigenous Data Governance, FAIR Guiding Principles for scientific data management and stewardship, California Consumer Privacy Act (CCPA). Each definition in the glossary may have additional references beyond those foundational documents.</p>

<hr>
## A {#a}


<a name="accessible_fair_principle"></a>
<div class="icon-container"><h3><img src="../img/Accessible (FAIR Principle).png" alt="Accessible (FAIR Principle) icon" class="icon-image" width="50" height="50">&nbsp;<term>
Accessible (FAIR Principle)</term></h3></div>
<id="definition"><i>
There should be clear means of viewing and using data, metadata, or infrastructure of interest. ‘Accessible’ is a key component of the FAIR Data Principles.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
FAIR Principles<br>
<strong>Related Terms:</strong>
Open data, Findable, Interoperable, Reusable, FAIR Principles</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
(1) The FAIR Principles definition: “Once the user finds the required data, they need to know how they can be accessed, possibly including authentication and authorisation.” (https://www.go-fair.org/fair-principles/)

“To be Accessible: A1. (meta)data are retrievable by their identifier using a standardized communications protocol; A1.1 the protocol is open, free, and universally implementable; A1.2 the protocol allows for an authentication and authorization procedure, where necessary; A2. metadata are accessible, even when the data are no longer available” (Wilkinson et al. 2016, Nature)
</li>
<li><strong>Agricultural Example:</strong>
Data should be “As Open as Possible, As Closed as Necessary”. Not all data needs to be or should be open. However, data that is open should be easy to see and use without complex or specialized protocols. Restricted data should be as open as possible.

The [USDA National Agricultural Library's Ag Data Commons](https://data.nal.usda.gov/) is an example of agricultural research data available through an open-access database. Since this database has public access, researchers and data users can resume and build upon it to further advance agricultural knowledge. </li>
<li><strong>Reference:</strong>
[FAIR Principles](https://www.go-fair.org/fair-principles/), [OpenAIRE: How to make your data FAIR](https://www.openaire.eu/how-to-make-your-data-fair) (referenced for example)</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="accountable"></a>
<div class="icon-container"><h3><img src="../img/Accountable.png" alt="Accountable icon" class="icon-image" width="50" height="50">&nbsp;<term>
Accountable</term></h3></div>
<id="definition"><i>
All parties involved in the collection, storage, and use of agricultural data must be held responsible for their engagement in data management and be subject to appropriate regulation and oversight. If data is found to be incorrect or misleading, reasonable steps are taken to correct it. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
GDPR<br>
<strong>Related Terms:</strong>
General Data Protection Regulation (GDPR); Data Transparency; Data Governance; Data Management</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
(1) The EU’s General Data Protection Regulation (GDPR) definition: accountability means to be “responsible for, and be able to demonstrate compliance with” principles and regulations of data protection
</li>
<li><strong>Agricultural Example:</strong>
Those involved in the collection, storage, and use of agricultural data, such as soil moisture levels, are responsible for ensuring the data are accurate and complete. They may be responsible for ensuring the data complies with regulations or standards.</li>
<li><strong>Reference:</strong>
EU AI Act: Principles for trustworthy AI, [Accountability | European Data Protection Supervisor](https://edps.europa.eu/data-protection/our-work/subjects/accountability_en)</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="accuracy"></a>
<div class="icon-container"><h3><img src="../img/Accuracy.png" alt="Accuracy icon" class="icon-image" width="50" height="50">&nbsp;<term>
Accuracy</term></h3></div>
<id="definition"><i>
Ensuring that data is correct and not misleading. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
GDPR<br>
<strong>Related Terms:</strong>
General Data Protection Regulation (GDPR); Accountable</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
(1) Article 5(1)(d) of the UK GDPR: “1. Personal data shall be: (d) accurate and, where necessary, kept up to date; every reasonable step must be taken to ensure that personal data that are inaccurate, having regard to the purposes for which they are processed, are erased or rectified without delay (‘accuracy’)”

(2) Without errors or mistakes

(3) Scientific, technical definition: the extent to which an observation aligns with the standard or correct value, often compared with precision

</li>
<li><strong>Agricultural Example:</strong>
Reasonable steps should be taken to ensure any data collected or held, such as agricultural data being collected from a sensor, is not incorrect or misleading. If it is found to be inaccurate or out of date, such as discovering a sensor is miscalibrated or data collected from remote sensing is dated and no longer represents current conditions, reasonable steps are taken to correct or erase the data.</li>
<li><strong>Reference:</strong>
[GDPR: Data protection principles principle (d): accuracy](https://ico.org.uk/for-organisations/guide-to-data-protection/guide-to-the-general-data-protection-regulation-gdpr/principles/accuracy/)</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="ag_data_transparent"></a>
<div class="icon-container"><h3><img src="../img/Ag Data Transparent.png" alt="Ag Data Transparent icon" class="icon-image" width="50" height="50">&nbsp;<term>
Ag Data Transparent</term></h3></div>
<id="definition"><i>
A certification and set of guidelines for companies collecting, storing, analyzing, and using agricultural data. Core principles include education, ownership, collection, access and control, notice, transparency and consistency, choice, portability, terms, and definitions. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Certifications and Guidelines<br>
<strong>Related Terms:</strong>
Data Transparency</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
Ag Data Transparent outlines 11 core principles: Education, Ownership, Collection, Access and Control, Notice, Transparency and Consistency, Choice, Portability, Terms and Definitions, Disclosure, Use and Sale Limitation, Data Retention and Availability, Contract Termination, Unlawful or Anti-Competitive Activities, Liability & Security Safeguards.</li>
<li><strong>Agricultural Example:</strong>
</li>
<li><strong>Reference:</strong>
[Ag Data Transparent](https://www.agdatatransparent.com/)</li>
<li><strong>Steward:</strong>
[Ag Data Transparency Evaluator, Inc](https://www.agdatatransparent.com/)</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="aggregate_data"></a>
<div class="icon-container"><h3><img src="../img/Aggregate Data.png" alt="Aggregate Data icon" class="icon-image" width="50" height="50">&nbsp;<term>
Aggregate Data</term></h3></div>
<id="definition"><i>
A combined dataset made up of a diversity of sources (e.g. sensors, systems, farmers, data platforms). This combination of datasets can provide additional value (e.g. benchmarking and analytics, identifying trends and timelines, etc.) to the data controller as compared to data from a single source. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Data<br>
<strong>Related Terms:</strong>
Anonymization; De-identification; Confidentiality; Personal Data; Agricultural Data; Purpose Limitation; Non-commercial Use; Commercial Use; Data Privacy</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
Aggregate data usually has different conditions in data use agreements than farm data or personal information because it is no longer associated with an individual or specific operation.
</li>
<li><strong>Agricultural Example:</strong>
Aggregate data on crop yield and regional prices could be used to determine if a new crop variety could be successful and profitable in a certain area, such as if the new crop has consistently high-quality yields and a premium price at market.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
<li><strong>Additional Notes:</strong>
It is common to remove personal information when aggregating large data sets, so that information can provide value to the public in an anonymized way.
</li>
</ul>
</details>
<a name="agricultural_advisor"></a>
<div class="icon-container"><h3><img src="../img/Agricultural Advisor.png" alt="Agricultural Advisor icon" class="icon-image" width="50" height="50">&nbsp;<term>
Agricultural Advisor</term></h3></div>
<id="definition"><i>
A professional who provides advice and support to farmers, ranchers, land stewards, and other actors in the agricultural sector. They may work independently or as part of an organization such as an extension service. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Roles<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
agricultural service provider</li>
<li><strong>Agricultural Example:</strong>
An agricultural advisor may have specific expertise in areas such as crop production, soil sciences, or agribusiness to provide advice on topics such as crop or practice selection, pest management, or financial management.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="agricultural_data"></a>
<div class="icon-container"><h3><img src="../img/Agricultural Data.png" alt="Agricultural Data icon" class="icon-image" width="50" height="50">&nbsp;<term>
Agricultural Data</term></h3></div>
<id="definition"><i>
A broad category of data types related to agricultural activities, including data about the land (e.g., soil and fertility data, geospatial data, etc.), data about on-farm crops and animals (e.g., seed type, yield, feed and health information about animals, etc.), data about or generated by farm equipment (e.g., model, fuel consumption, yield maps, etc.), and other information about farm management (e.g., commodity price, farm revenue, employment, etc.) </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Data<br>
<strong>Related Terms:</strong>
Data, personal information, aggregate data</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
Most data-related policy focuses on “personal information” (including the GDPR and CCPA). This leaves the connection to regulating or governing agricultural data ambiguous. Agricultural data can sometimes also be considered  “personal data” when it makes a specific person or operation identifiable. 
</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="anonymization"></a>
<div class="icon-container"><h3><img src="../img/Anonymization.png" alt="Anonymization icon" class="icon-image" width="50" height="50">&nbsp;<term>
Anonymization</term></h3></div>
<id="definition"><i>
All personal or identifiable information is removed from the dataset, such that it is impossible for any user to gain insights about a discrete individual. It is an expectation of anonymization that the data could never be re-identified.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
<br>
<strong>Related Terms:</strong>
de-identification, confidentiality; Personal Data; Data Privacy</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
A farmer shares soil test results with a third party for analysis. To protect the farmer’s privacy, the service provider can remove identifying information such as the name and location of the farmers and assign a unique identifier to the data instead. The identifier can be used to link the data back to the farm, but the farmer’s identity is not disclosed to anyone else. The farmer can then benefit from the analysis of the soil sample without worrying that their personal information is shared with others.

In agricultural contexts, additional information about the farm operation may need to be removed or concealed to ensure that the associated land stewards cannot be identified.
</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="australian_farm_data_code"></a>
<div class="icon-container"><h3><img src="../img/Australian Farm Data Code.png" alt="Australian Farm Data Code icon" class="icon-image" width="50" height="50">&nbsp;<term>
Australian Farm Data Code</term></h3></div>
<id="definition"><i>
Developed by the National Farmers’ Federation in Australia, this code offers guidelines for those managing data on farmers’ behalf. “The Code is intended to inform the data management policies of service providers…It is also a yardstick by which farmers can evaluate the policies of those providers.”</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Certifications and Guidelines<br>
<strong>Related Terms:</strong>
Data Governance; Data Management; Agricultural Data; General Data Protection Regulation (GDPR); California Consumer Privacy Act (CCPA)</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
Objectives of the Australian Farm Data Code:
“The purpose of this Code is to facilitate innovation in agriculture by ensuring farmers have confidence in how their data is collected, used and shared. The Code does this by establishing leading principles for the collection and use of farm data. Specifically, the Code aims to:

a. raise awareness around the collection, use and sharing of farm data;

b. improve transparency, clarity and honesty in the way farm data is collected, used and shared;

c. encourage the fair and equitable collection, use and sharing of farm data in a way that benefits farmers and Australian agriculture;

d. build trust and confidence in the way farm data is collected, used and shared so that, where appropriate, farm data can be utilised in ways that bring benefits to Australian agriculture; and,

e. allow flexible implementation, so that providers can establish appropriate practices around farm data collection, use and sharing” [Australian Farm Data Code - National Farmers' Federation](https://nff.org.au/programs/australian-farm-data-code/)
</li>
<li><strong>Reference:</strong>
[Australian Farm Data Code - National Farmers' Federation](https://nff.org.au/programs/australian-farm-data-code/)</li>
<li><strong>Steward:</strong>
[National Farmers’ Federation](https://nff.org.au/)</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>

## C {#c}

<a name="california_consumer_privacy_act_ccpa"></a>
<div class="icon-container"><h3><img src="../img/California Consumer Privacy Act (CCPA).png" alt="California Consumer Privacy Act (CCPA) icon" class="icon-image" width="50" height="50">&nbsp;<term>
California Consumer Privacy Act (CCPA)</term></h3></div>
<id="definition"><i>
A data privacy law that went into effect in 2020, giving California residents the right to ask a business to disclose what personal information they have about the resident, what they do with that information, to correct inaccuracies, and to request that their information be deleted or not sold to third parties. Businesses subject to CCPA have responsibilities to respond to consumer requests to these rights and provide certain notices. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Regulations<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
Rights under the CCPA:

“<strong>Right to know:</strong> You can request that a business disclose to you: (1) the categories and/or specific pieces of personal information they have collected about you, (2) the categories of sources for that personal information, (3) the purposes for which the business uses that information, (4) the categories of third parties with whom the business discloses the information, and (5) the categories of information that the business sells or discloses to third parties. You can make a request to know up to twice a year, free of charge.
<strong>Right to delete:</strong> You can request that businesses delete personal information they collected from you and tell their service providers to do the same, subject to certain exceptions (such as if the business is legally required to keep the information).
<strong>Right to opt-out of sale or sharing:</strong> You may request that businesses stop selling or sharing your personal information (“opt-out”), including via a user-enabled global privacy control. Businesses cannot sell or share your personal information after they receive your opt-out request unless you later authorize them to do so again.
<strong>Right to correct:</strong> You may ask businesses to correct inaccurate information that they have about you.
<strong>Right to limit use and disclosure of sensitive personal information:</strong> You can direct businesses to only use your sensitive personal information (for example, your social security number, financial account information, your precise geolocation data, or your genetic data) for limited purposes, such as providing you with the services you requested.”</li>
<li><strong>Reference:</strong>
[California Consumer Privacy Act (CCPA)](https://oag.ca.gov/privacy/ccpa)</li>
<li><strong>Steward:</strong>
State of California Department of Justice</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="care_principles_of_indigenous_data_governance"></a>
<div class="icon-container"><h3><img src="../img/CARE Principles of Indigenous Data Governance.png" alt="CARE Principles of Indigenous Data Governance icon" class="icon-image" width="50" height="50">&nbsp;<term>
CARE Principles of Indigenous Data Governance</term></h3></div>
<id="definition"><i>
The CARE principles, stewarded by The Global Indigenous Data Alliance, outline four related requirements for data management to support Indigenous Data Sovereignty and self-determination: Collective Benefits, Authority to Control, Responsibility, Ethics. The CARE Principles were designed to be complementary with FAIR principles, but they are not necessarily applied together. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
#BeFAIRandCARE, Care, Fair, Principles, Collective Benefit, Authority to Control, Responsibility, Ethics, OCAP, Indigenous Data Governance, Indigenous Data Sovereignty, UNDRIP<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
When working with Indigenous communities, knowledges, and data, there are additional considerations for ethical and responsible data governance. 

Note: Non-Indigenous people and organizations can learn from the CARE Principles for developing their own protocols, but the Global Indigenous Data Alliance developed and stewards CARE to protect Indigenous Peoples’ data sovereignty and rights to self-determination.
</li>
<li><strong>Agricultural Example:</strong>
Do the data management and governance approaches for agricultural data acknowledge and address power differentials and historical contexts (e.g., colonialism)?</li>
<li><strong>Example / Anecdote:</strong>
CARE vs. FAIR: "Tribal databases and Indigenous Content Management Systems (e.g. Mukurtu.org) hold tribal data using protocols consistent with tribal values and worldviews, thus employing CARE. However, these collections are generally not consistent with FAIR principles and require enriched metadata and protocols. The Integrated Data Infrastructure (IDI) in New Zealand has developed a data access protocol called Ngā Tikanga Paihere which is based on Indigenous concepts and values consistent with CARE as well as ensuring the data are also FAIR ([Ngā Tikanga Paihere - data.govt.nz](https://data.govt.nz/use-data/data-ethics/nga-tikanga-paihere/)). Application of CARE with FAIR requires a clear set of criteria and tools such as the FAIR Data Maturity Model. Compiling existing and creating new tools and criteria for implementing the CARE Principles are needed to achieve data that are FAIR with CARE." (Carroll et al 2021, 3)

</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
[The Global Indigenous Data Alliance](https://www.gida-global.org/care)</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="commercial_use"></a>
<div class="icon-container"><h3><img src="../img/Commercial Use.png" alt="Commercial Use icon" class="icon-image" width="50" height="50">&nbsp;<term>
Commercial Use</term></h3></div>
<id="definition"><i>
The data can be used for a commercial, or economic, purpose (e.g., as part of a transaction, as a form of capital, or to inform R&D, for advertising)</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
Using agricultural data to produce insights in a commercial farm management platform sold as a service to farmers.

Looking at the Terms of Use and Privacy Policies for farm management tools (e.g., Climate FieldView), there are many commercial uses outlined (e.g., providing paid services, improving the company's products and services, R&D, targeted online advertising within and outside of the platform, etc.).</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="conditional_use"></a>
<div class="icon-container"><h3><img src="../img/Conditional Use.png" alt="Conditional Use icon" class="icon-image" width="50" height="50">&nbsp;<term>
Conditional Use</term></h3></div>
<id="definition"><i>
Use of the data is subject to certain rules or constraints (e.g., not for commercial purposes)</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
a farmer may be comfortable sharing their de-identified agricultural data for research use by an academic or government institution, but not for companies to use for commercial purposes, such as targeted marketing of agricultural input products.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="confidentiality"></a>
<div class="icon-container"><h3><img src="../img/Confidentiality.png" alt="Confidentiality icon" class="icon-image" width="50" height="50">&nbsp;<term>
Confidentiality</term></h3></div>
<id="definition"><i>
Protection of data from unauthorized access or use.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
GDPR: Data protection principles<br>
<strong>Related Terms:</strong>
anonymization; security</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions:</strong>
(1) Information is protected and not to be shared


(2) The Principle of “integrity and confidentiality” in the EU’s General Data Protection Regulation (GDPR): “personal information shall be […] (f) processed in a manner that ensures appropriate security of the personal data, including protection against unauthorised or unlawful processing and against accidental loss, destruction or damage, using appropriate technical or organisational measures” (Art. 5 GDPR)

</li>
<li><strong>Agricultural Example:</strong>
In many jurisdictions, it is a legal requirement for businesses to outline how user data will be kept confidential. When a land steward is reviewing the data use agreement for a new digital tool for their operation, they may want to know what protections are in place to keep their personal information and agricultural data safe from use by third parties.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="consent"></a>
<div class="icon-container"><h3><img src="../img/Consent.png" alt="Consent icon" class="icon-image" width="50" height="50">&nbsp;<term>
Consent</term></h3></div>
<id="definition"><i>
Freely given and informed statement that signifies agreement to an action related to an individual’s or organization’s data and their operations. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
GDPR: Data protection principles<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions:</strong>
(1) To give permission for something to happen

(2) The EU’s General Data Protection Regulation (GDPR) definition: “‘consent’ of the data subject means any freely given, specific, informed and unambiguous indication of the data subject’s wishes by which he or she, by a statement or by a clear affirmative action, signifies agreement to the processing of personal data relating to him or her;”
</li>
<li><strong>Agricultural Example:</strong>
When using a digital farm management tool, there are data use agreements to outline the processes of data collection, use, storage, and portability. In most cases, a land steward using the tool (e.g., downloading an app, inputting and collecting data) implies legal agreement or “consent” to the tools’ terms and conditions.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>

## D {#d}

<a name="data"></a>
<div class="icon-container"><h3><img src="../img/Data.png" alt="Data icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data</term></h3></div>
<id="definition"><i>
Digital, electronic, or physical representation of information</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Data<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_architect"></a>
<div class="icon-container"><h3><img src="../img/Data Architect.png" alt="Data Architect icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Architect</term></h3></div>
<id="definition"><i>
The professional(s) responsible for designing, creating, integrating, and managing data management systems, and/or producing products that utilizes data for analytics. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
roles<br>
<strong>Related Terms:</strong>
Data Controller; Data Fiduciary; Data Originator; Data Processor; Data Steward; Data Subject; Data User; Data Management</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
A data architect may be the person or group that designs and manages the database structure for a farm management software application, such as a software platform that enables farmers to track crop yield, monitor soil quality, and manage equipment. The data architect may design a database structure that can handle large amounts of data and manage appropriate fields and relationships between data.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_controller"></a>
<div class="icon-container"><h3><img src="../img/Data Controller.png" alt="Data Controller icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Controller</term></h3></div>
<id="definition"><i>
A person or group that decides why and how data is collected, processed, and used. The data controller is often the data subject unless they have authorized and reassigned control to a proxy. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
<br>
<strong>Related Terms:</strong>
Data Architect; Data Fiduciary; Data Originator; Data Processor; Data Steward; Data Subject; Data User; Data Management</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
The data ‘controller’ is the natural or legal person, public authority, agency, or other body which, alone or jointly with others, determines the purposes and means of the processing of personal data; where the purposes and means of such processing are determined by Union or Member State law, the controller or the specific criteria for its nomination may be provided for by Union or Member State law; [Art. 4 GDPR – Definitions - General Data Protection Regulation (GDPR)](https://gdpr-info.eu/art-4-gdpr/)</li>
<li><strong>Agricultural Example:</strong>
A farmer may act as the data controller, deciding how data about the farm, such as soil health and crop yields, is accessed, used and shared. They may also assign these decisions to another individual or group serving as a proxy.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_fiduciary"></a>
<div class="icon-container"><h3><img src="../img/Data Fiduciary.png" alt="Data Fiduciary icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Fiduciary</term></h3></div>
<id="definition"><i>
A person or group that has access to and manages data on behalf of the data originator. The data fiduciary is legally bound to act ethically and in the best interest of their client.
</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Roles<br>
<strong>Related Terms:</strong>
Data Architect; Data Controller; Data Originator; Data Processor; Data Steward; Data Subject; Data User; Data Management</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
“Data Fiduciary” means any person who alone or in conjunction with other persons determines the purpose and means of processing of personal data” [The Digital Personal Data Protection Bill, 2022](https://www.meity.gov.in/writereaddata/files/The%20Digital%20Personal%20Data%20Potection%20Bill%2C%202022_0.pdf)</li>
<li><strong>Agricultural Example:</strong>
a data fiduciary could be a technical assistance provider who collects, processes, and stores data related to a farmer’s agricultural operations on their behalf. This individual has an obligation to use the data only for the purpose it was collected.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_governance"></a>
<div class="icon-container"><h3><img src="../img/Data Governance.png" alt="Data Governance icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Governance</term></h3></div>
<id="definition"><i>
Data governance involves both: 
(1) the technological and logistical management of data within an application or organization (i.e., directing data collection, analysis, storage, and security) and
(2) the political structures and systems that direct the data management within or across organizations.
 </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
<br>
<strong>Related Terms:</strong>
Data Management; Data Sovereignty</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
</li>
<li><strong>Reference:</strong>
<li>Micheli, M., Ponti, M., Craglia, M., & Berti Suman, A. (2020). Emerging models of data governance in the age of datafication. Big Data and Society, 7(2). https://doi.org/10.1177/2053951720948087</li>
<li>Micheli, M. (2020). Emerging models of data governance and the politics of data. https://ec.europa.eu/jrc/communities/sites/jrccties/files/micheli.pdf </li>
<li>Cue, R., Doornink, M., George, R., Griffiths, B., Jorgensen, M. W., Rogers, R., Saha, A., Taysom, K., Cabrera, V. E., Wangen, S. R., & Fadul‐pacheco, L. (2021). Data governance in the dairy industry. Animals, 11(10), 1–11. https://doi.org/10.3390/ani11102981 </li>
<li>https://en.wikipedia.org/wiki/Data_governance </li>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_justice"></a>
<div class="icon-container"><h3><img src="../img/Data Justice.png" alt="Data Justice icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Justice</term></h3></div>
<id="definition"><i>
Acknowledging and creating action around the way data collection and dissemination have previously and continue to harm historically marginalized communities, data justice recognizes the relationship between data and social justice and aims to represent diverse communities and promote autonomy and trust. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
<br>
<strong>Related Terms:</strong>
fairness, CARE Principles</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
“To speak of data justice is thus to recognise not only how data, its collection and use, increasingly impacts on society, but also that datafication is enabled by particular forms of political and economic organization that advance a normative vision of how social issues should be understood and resolved” (Dencik & Sanchez-Monedero 2022)
</li>
<li><strong>Agricultural Example:</strong>
"Data justice can look like many different things, it is above all process-driven and responsive to the community. A data-justice informed project should: 
1. Represent and make visible the challenges and strengths of the community. 
2. Treat data in ways that promote community self-determination, which include considerations of consent and ownership of data. 
3. Pro-actively consider potential harm to the community and work to mitigate it. 
4. Make critical consideration of the value of invisibility and disengagement for certain communities." (from the UBC ORICE, Gender+Collective "Community-Based Research & Data Justice Resource Guide)
</li>
<li><strong>Reference:</strong>
<li>[Research & Data Justice — Coalition of Communities of Color](https://www.coalitioncommunitiescolor.org/-why-research-data-justice), [Data Justice Lab](https://datajusticelab.org/about/)</li> 
<li>Dencik, L., & Sanchez-Monedero, J. (2022). Data Justice. Internet Policy Review, 11(1), 1–16. https://doi.org/10.14763/2022.1.1615</li> 
<li>Taylor, L. (2017). What is data justice? The case for connecting digital rights and freedoms globally. Big Data and Society, 4(2), 1–14. https://doi.org/10.1177/2053951717736335</li> 
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_management"></a>
<div class="icon-container"><h3><img src="../img/Data Management.png" alt="Data Management icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Management</term></h3></div>
<id="definition"><i>
The process of collecting, organizing, analyzing, and accessing data.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
GDPR: Data protection principles<br>
<strong>Related Terms:</strong>
Data Architect; Data Controller; Data Fiduciary; Data Originator; Data Processor; Data Steward; Data Subject; Data User</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
Data management may include collecting farm-level data like soil samples and crop information, overseeing how and where that information is stored and organized, and determining the means for accessing and analyzing that data in a secure way, for future use.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_minimization"></a>
<div class="icon-container"><h3><img src="../img/Data Minimization.png" alt="Data Minimization icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Minimization</term></h3></div>
<id="definition"><i>
The practice of limiting the collection of personal data. To achieve data minimization, the data being processed should be adequate and relevant to fulfill the stated purpose and not hold more data than is needed. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
GDPR: Data protection principles<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
A land steward, data originator, or another party can choose to collect or report only data that is essential to a given research objective. For instance, collecting or reporting data to understand how soil conditions affect crop yield does not require the collection of personal data about farms or their employees.</li>
<li><strong>Reference:</strong>
[European Data Protection Supervisor](https://edps.europa.eu/data-protection/data-protection/glossary/d_en#:~:text=The%20principle%20of%20%E2%80%9Cdata%20minimisation,necessary%20to%20fulfil%20that%20purpose)</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_originator"></a>
<div class="icon-container"><h3><img src="../img/Data Originator.png" alt="Data Originator icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Originator</term></h3></div>
<id="definition"><i>
The person or group purposefully providing data. At OpenTEAM, we believe the land steward should be legally and procedurally recognized as the data originator.
</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Roles<br>
<strong>Related Terms:</strong>
Data Architect; Data Controller; Data Fiduciary; Data Processor; Data Steward; Data Subject; Data User; Data Management
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
According to the [EU Code of Conduct on Agricultural Data Sharing by Contractual Agreement](https://fefac.eu/wp-content/uploads/2020/07/eu_code_of_conduct_on_agricultural_data_sharing-1.pdf), the data originator is “the person or entity that can claim the exclusive right to license access to the data and control its downstream use or re-use, i.e., the party that the data is attributed to. The data originator of all the data generated during the operation is the one who has created/collected this data either by technical means (e.g., agricultural machinery, electronic data processing programs), but themselves or who has commissioned data providers for this purpose.”
</li>
<li><strong>Agricultural Example:</strong>
A farmer who collects data about the land they are farming on, such as crop variety, crop yields, and soil conditions.</li>
<li><strong>Reference:</strong>
[EU Code of Conduct on Agricultural Data Sharing by Contractual Agreement](https://fefac.eu/wp-content/uploads/2020/07/eu_code_of_conduct_on_agricultural_data_sharing-1.pdf)
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_ownership"></a>
<div class="icon-container"><h3><img src="../img/Data Ownership.png" alt="Data Ownership icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Ownership</term></h3></div>
<id="definition"><i>
</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
GDPR<br>
<strong>Related Terms:</strong>
Data Architect; Data Controller; Data Fiduciary; Data Originator; Data Steward; Data Subject; Data User; Data Management</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
A data processor may be a farm management software company that provides tools for managing farm data such as crop yields and soil quality.</li>
<li><strong>Reference:</strong>
Art. 4 GDPR – Definitions - General Data Protection Regulation (GDPR)</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_portability"></a>
<div class="icon-container"><h3><img src="../img/Data Portability.png" alt="Data Portability icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Portability</term></h3></div>
<id="definition"><i>
The ability of an individual or organization to 'move' their data from one place, platform, or software to another</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Data<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
Data portability includes the ability to easily export data, such as yield maps or soil test results, from one platform and import it into another without losing information or needing to manually enter data again.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_privacy"></a>
<div class="icon-container"><h3><img src="../img/Data Privacy.png" alt="Data Privacy icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Privacy</term></h3></div>
<id="definition"><i>
The rules for how, and by whom, data can be collected, shared, and used, protecting personal data from unauthorized access and use. It further refers to handling data in compliance with relevant data protection laws and regulations. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
<br>
<strong>Related Terms:</strong>
Data Governance; Data Management; Personal Data; Data Transparency; Confidentiality</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>

- Corporate proprietary and open-source tools alike will have data privacy policies if they handle personal data, as required by law. 

- For example, Climate FieldView defines their policy on data privacy: “This Privacy Statement explains what information we collect about you, what we do with it, who we share it with and how you can control it in connection with our Climate products, websites, applications, or other digital services (the “Climate Services”).”

</li>
<li><strong>Agricultural Example:</strong>
Data privacy is important in the case of a farmer processing personal employee data to comply with payroll requirements. The farmer needs to ensure the personal data is collected, processed, and stored in compliance with data privacy principles and regulations to protect the employees' privacy.</li>
<li><strong>Reference:</strong>
[Data Privacy Guide: Definitions, Explanations and Legislation](https://www.varonis.com/blog/data-privacy)</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_processing"></a>
<div class="icon-container"><h3><img src="../img/Data Processing.png" alt="Data Processing icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Processing</term></h3></div>
<id="definition"><i>
The ability of an individual or organization to 'move' their data from one place, platform, or software to another</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Data<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
The data processor has the role of processing data. According to the [EU Code of Conduct on Agricultural Data Sharing by Contractual Agreement](https://fefac.eu/wp-content/uploads/2020/07/eu_code_of_conduct_on_agricultural_data_sharing-1.pdf), processing includes “any operation or set of operations that is performed on data or on datasets, whether by automated means or not, such as collection, recording, organisation, structuring, storage, adaptation or alteration, retrieval, consultation, use, disclosure by transmission, dissemination or other methods of making the data available, alignment or combination, restriction, erasing or destruction.” 

</li>
<li><strong>Agricultural Example:</strong>
Data portability may be the ability for a data subject or controller to easily export data, such as yield maps or soil test results, from one platform and import it into another without losing information or needing to manually enter data again.</li>
<li><strong>Reference:</strong>
[Art. 4 GDPR – Definitions - General Data Protection Regulation (GDPR)](https://gdpr-info.eu/art-4-gdpr/)
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_processor"></a>
<div class="icon-container"><h3><img src="../img/Data Processor.png" alt="Data Processor icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Processor</term></h3></div>
<id="definition"><i>
The person or group responsible for handling or performing actions on data. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
GDPR<br>
<strong>Related Terms:</strong>
Data Architect; Data Controller; Data Fiduciary; Data Originator; Data Steward; Data Subject; Data User; Data Management</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
A data processor may be a farm management software company that provides tools for managing farm data such as crop yields and soil quality.</li>
<li><strong>Reference:</strong>
Art. 4 GDPR – Definitions - General Data Protection Regulation (GDPR)</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_sharing"></a>
<div class="icon-container"><h3><img src="../img/Data Sharing.png" alt="Data Sharing icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Sharing</term></h3></div>
<id="definition"><i>
The process of making data available and accessible to others</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Data<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
A group of farmers in a region agree to share crop yield data with one another, to compare yields, identify best practices, and make more informed decisions for their farms. This could take place through a centralized platform or a more informal setting .</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_sovereignty"></a>
<div class="icon-container"><h3><img src="../img/Data Sovereignty.png" alt="Data Sovereignty icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Sovereignty</term></h3></div>
<id="definition"><i>
An individual’s ability to create, control, and manage their own data. It ensures that the individual or community, about whom data is collected, has knowledge of and meaningful consent over how that information is used and shared by others, with tools and resources to control, interpret, and act on their own data.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
<br>
<strong>Related Terms:</strong>
CARE Principles; Data Governance; Data Subject; Data Steward; Data User; Data Ownership; Data Controller</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
Addresses who has control over, ownership, and manages of data or databases and under what conditions (e.g., laws, agreements, etc.). // NOTE: this term is used differently by different groups and lacks a universal definition.</li>
<li><strong>Agricultural Example:</strong>
Organizations such as Global Indigenous Data Alliance (GIDA) have worked with indigenous communities to develop data collection and management strategies that prioritize community control and ownership of data. For example, instead of relying on external researchers or organizations to collect and analyze data about their farming practices, indigenous farmers are working with GIDA to collect and manage their own data, using methods that are culturally appropriate and aligned with their values. The [CARE Principles](https://docs.google.com/document/d/16nBSKVeqPQDWZ4zSeLjx-DT_cC2xt1083GSM1TBWw2w/edit#bookmark=id.ry5ts9kupj01) emphasize the importance of free, prior, and informed consent in an effort to ensure indigenous peoples are not subject to exploitation or harm as a result of data collection and use.</li>
<li><strong>Reference:</strong>

- Hummel, Patrik, Matthias Braun, Max Tretter, and Peter Dabrock. 2021. “Data Sovereignty: A Review.” Big Data and Society 8 (1). https://doi.org/10.1177/2053951720982012.
- Irion, Kristina (2012-12-01). "Government Cloud Computing and National Data Sovereignty". Policy & Internet. 4 (3–4): 40–71. doi:10.1002/poi3.10. ISSN 1944-2866
Gilmore, David, DataFleets, "Google Scrapped Cloud Initiative in China, Other Markets", Bloomberg News". July 8, 2020.
- [Global Indigenous Data Alliance](https://www.gida-global.org/)
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_steward"></a>
<div class="icon-container"><h3><img src="../img/Data Steward.png" alt="Data Steward icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Steward</term></h3></div>
<id="definition"><i>
The person or group responsible for managing and maintaining data, ensuring it is accurate, complete, secure, and adheres to governmental and organizational policy. The data steward may also contribute to data management to support the intended (re)use of data.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Roles<br>
<strong>Related Terms:</strong>
Data Architect; Data Controller; Data Fiduciary; Data Originator; Data Processor; Data Subject; Data User</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
A food company that manages and maintains data from its producer supply chain is considered a data steward. Data accountability and data protection are closely tied to a data steward’s responsibilities.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_storage"></a>
<div class="icon-container"><h3><img src="../img/Data Storage.png" alt="Data Storage icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Storage</term></h3></div>
<id="definition"><i>
The process of saving information, including how and where it is stored, ensuring that it is accessible and retrievable when needed. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
A data controller may choose which ag data platform or software to store farm data on, depending on what is most suitable for their use.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_subject"></a>
<div class="icon-container"><h3><img src="../img/Data Subject.png" alt="Data Subject icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Subject</term></h3></div>
<id="definition"><i>
The person, entity, or thing, that is the focus of inquiry. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
GDPR<br>
<strong>Related Terms:</strong>
Data Architect; Data Controller; Data Fiduciary; Data Originator; Data Processor; Data Steward; Data Subject; Data User; Data Management
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
According to the General Data Protection Regulation (GDPR), a data subject is an individual who can be identified directly or indirectly by reference to a personal identifier such as a name, an identification number, location data, an online identifier, or one or more factors specific to the physical, physiological, genetic, mental, economic, cultural, or social identity of that individual.</li>
<li><strong>Agricultural Example:</strong>
A data subject may be an individual farmer, rancher, or land steward. It may also be crops, livestock, or the land itself. Data collected on the data subject may include name, address, and contact information or land attributes like crop data.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_transparency"></a>
<div class="icon-container"><h3><img src="../img/Data Transparency.png" alt="Data Transparency icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data Transparency</term></h3></div>
<id="definition"><i>
The condition with which Individuals are informed and have access to adequate and comprehensive information regarding their data. This includes what is being collected, how it is used, and who has access. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
EU AI Act: Principles for trustworthy AI, GDPR: Data protection principles<br>
<strong>Related Terms:</strong>
Ag Data Transparent, Data Privacy; Data Management; Data Governance</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>

- Ability to easily access and work with data regardless of source or location
- Could include transparency on the conditions and protocol for data collection to ascertain the accuracy and credibility of the information

</li>
<li><strong>Agricultural Example:</strong>
A software provider may include features to allow farmers to view their data in a clear and understandable format and provide an explanation of how the software collects, uses, and shares data.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="data_user"></a>
<div class="icon-container"><h3><img src="../img/Data User.png" alt="Data User icon" class="icon-image" width="50" height="50">&nbsp;<term>
Data User</term></h3></div>
<id="definition"><i>
A person or group that receives data from the data originator or data provider, via an agreement with the data originator.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Roles<br>
<strong>Related Terms:</strong>
Data Architect; Data Controller; Data Fiduciary; Data Originator; Data Processor; Data Steward; Data Subject
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
A data user could be a farm management software company that uses data collected from farmers (such as crop yields, weather conditions, and fertilizer usage) to provide analytics and insights to improve the farmer’s operations.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="deidentification"></a>
<div class="icon-container"><h3><img src="../img/De-identification.png" alt="De-identification icon" class="icon-image" width="50" height="50">&nbsp;<term>
De-identification</term></h3></div>
<id="definition"><i>
Removing all or a subset of personally identifying information from a dataset to prevent users from deducting personal information about any individual with the available data (e.g., removing names, addresses, etc.). De-identification is a strategy to protect an individual's privacy and safety while preserving the usefulness or utility of the data(set) as much as possible.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
<br>
<strong>Related Terms:</strong>
anonymization</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
To protect a farmer’s privacy, in the case of a farm wishing to share yield data with researchers, data can be de-identified by removing personally identifiable information (such as names and addresses of the farmers who grew the crops), before disseminating any data with researchers. This allows the creation of valuable insights without compromising the privacy of farmers. In agricultural contexts, additional information about the farm operation (e.g., location) may need to be removed or concealed to ensure that the associated land stewards cannot be identified. </li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>

## F {#f}

<a name="fair_principles"></a>
<div class="icon-container"><h3><img src="../img/FAIR Principles.png" alt="FAIR Principles icon" class="icon-image" width="50" height="50">&nbsp;<term>
FAIR Principles</term></h3></div>
<id="definition"><i>
Guidelines for 'good data management,' which improve the discovery and (re)use of scholarly data by humans and computers (e.g., machine learning, algorithms). The four foundational principles are:

- <strong>Findable</strong> Data (or any digital object), metadata (i.e., information about that digital object), and infrastructure (e.g., data registered or indexed in a searchable resource) should be easy to find for both humans and computers
- <strong>Accessible</strong> Once found, there should be clear means of accessing data, metadata, or infrastructure of interest
- <strong>Interoperable</strong> Data should work in conjunction with applications or workflows for analysis, storage, and processing
- <strong>Reusable</strong> Metadata and data should be well-described so that they can be (re)used, replicated, or combined in different settings.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
FAIR, Principles,<br>
<strong>Related Terms:</strong>
Findable, Accessible, Interoperable, Reusable</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
The creation of a digital repository of soil data that adheres to the FAIR principles.

- Findable: Identifiers are assigned to each dataset and descriptive metadata, such as data and location, is tied to the data.
- Accessible: A user-friendly interface allows farmers and researchers to search and retrieve the data easily.
- Interoperable: Standard formats are used to allow the data to be easily integrated with soil data from different sources.
- Reusable: Open access licensing could be used to allow farmers and researchers to use the data freely, which may lead to improved agricultural practices. 
</li>
<li><strong>Reference:</strong>
[FAIR Principles](https://www.go-fair.org/fair-principles/)</li>
<li><strong>Steward:</strong>
[GO FAIR](https://www.go-fair.org/)</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="fairness"></a>
<div class="icon-container"><h3><img src="../img/Fairness.png" alt="Fairness icon" class="icon-image" width="50" height="50">&nbsp;<term>
Fairness</term></h3></div>
<id="definition"><i>
"Fairness</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
GDPR: Data protection principles<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="findable_fair_principle"></a>
<div class="icon-container"><h3><img src="../img/Findable (FAIR Principle).png" alt="Findable (FAIR Principle) icon" class="icon-image" width="50" height="50">&nbsp;<term>
Findable (FAIR Principle)</term></h3></div>
<id="definition"><i>
Data, metadata, and infrastructure should be easy to find for both humans and computers. It is a key component of the FAIR Data Principles.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
FAIR Principles<br>
<strong>Related Terms:</strong>
Accessible, interoperable, reusable</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
(1) The FAIR Principles definition: “The first step in (re)using data is to find them. Metadata and data should be easy to find for both humans and computers. Machine-readable metadata are essential for automatic discovery of datasets and services.” ([FAIR Principles](https://www.go-fair.org/fair-principles/))
</li>
<li><strong>Agricultural Example:</strong>
Publishing a dataset on crop yields in a specific region using metadata makes the data easily discoverable to other researchers or data users, increasing its potential for reuse and impact.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
[FAIR Principles](https://www.go-fair.org/fair-principles/)</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>

## G {#g}

<a name="general_data_protection_regulation_gdpr"></a>
<div class="icon-container"><h3><img src="../img/General Data Protection Regulation (GDPR).png" alt="General Data Protection Regulation (GDPR) icon" class="icon-image" width="50" height="50">&nbsp;<term>
General Data Protection Regulation (GDPR)</term></h3></div>
<id="definition"><i>
The European Union's law on protecting personal data and privacy of all people in the Europe Union, whether or not the data management takes place in Europe.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
GDPR<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
According to Article 1 of the GDPR ‘Subject-matter and objectives’, the GDPR seeks to do the following:

“1. This Regulation lays down rules relating to the protection of natural persons with regard to the processing of personal data and rules relating to the free movement of personal data.
2. This Regulation protects fundamental rights and freedoms of natural persons and in particular their right to the protection of personal data.
3. The free movement of personal data within the Union shall be neither restricted nor prohibited for reasons connected with the protection of natural persons with regard to the processing of personal data.”
</li>
<li><strong>Agricultural Example:</strong>
A farm in the European Union collecting personal data from its workers, such as names and addresses, ensuring they are obtaining consent for use of that data. They must follow GDPR guidelines for storing and using that data.</li>
<li><strong>Reference:</strong>
[GDPR](https://gdpr-info.eu/)</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>

## I {#i}

<a name="interoperable_fair_principle"></a>
<div class="icon-container"><h3><img src="../img/Interoperable (FAIR Principle).png" alt="Interoperable (FAIR Principle) icon" class="icon-image" width="50" height="50">&nbsp;<term>
Interoperable (FAIR Principle)</term></h3></div>
<id="definition"><i>
A dataset’s ability to be aggregated with other datasets in meaningful ways and to work with applications and workflows. For systems and softwares, interoperable refers to these systems’ ability to exchange and make use of information or operate in conjunction with one another. It is a key component of the FAIR Data Principles.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
FAIR Principles<br>
<strong>Related Terms:</strong>
Finable, Accessible, Reusable</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
(1) The FAIR Principles definition: “The data usually need to be integrated with other data. In addition, the data need to interoperate with applications or workflows for analysis, storage, and processing.” ([FAIR Principles](https://www.go-fair.org/fair-principles/))
</li>
<li><strong>Agricultural Example:</strong>
Interoperability may be the ability for a farmer acting as the data controller to input farm data once and be able to use it across multiple platforms for different benefits such as benchmarking or gaining access to agricultural transition incentives.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
[FAIR Principles](https://www.go-fair.org/fair-principles/)</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>

## L {#l}

<a name="land_stewards"></a>
<div class="icon-container"><h3><img src="../img/Land Stewards.png" alt="Land Stewards icon" class="icon-image" width="50" height="50">&nbsp;<term>
Land Stewards</term></h3></div>
<id="definition"><i>
A person or group who works with or adjacent to the land, regardless of ownership, with the goal of implementing environmentally, culturally, and socially responsible agricultural practices. A land steward could be someone working directly on the land, such as a farmer, or it could be an individual who supports those working on the land, such as a technical assistance provider.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
A land steward could be a farmer who manages rented land from a landowner to grow crops. They may choose to implement practices such as crop rotation, cover cropping, or natural fertilizers to create a productive and sustainable operation for the environment, farmer, and community.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="lawfulness"></a>
<div class="icon-container"><h3><img src="../img/Lawfulness.png" alt="Lawfulness icon" class="icon-image" width="50" height="50">&nbsp;<term>
Lawfulness</term></h3></div>
<id="definition"><i>
Establishing an appropriate basis for the processing of data that is in compliance with relevant laws and regulations. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
Under the GDPR, Article 6 outlines the six lawful bases for processing personal data, which includes "processing is necessary for the performance of a contract to which the data subject is party or in order to take steps at the request of the data subject prior to entering into a contract."</li>
<li><strong>Agricultural Example:</strong>
Lawfulness could apply to a provider collecting and processing personal data from farmers. If these farmers are in the European Union, they are protected under the GDPR, and the provider may only collect this data if the farmer has given explicit consent.</li>
<li><strong>Reference:</strong>
GDPR Data Protection Principles</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>

## M {#m}

<a name="machine_readable"></a>
<div class="icon-container"><h3><img src="../img/Machine Readable.png" alt="Machine Readable icon" class="icon-image" width="50" height="50">&nbsp;<term>
Machine Readable</term></h3></div>
<id="definition"><i>
Data in a form that a computer, artificial intelligence, or algorithm can process</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Data use<br>
<strong>Related Terms:</strong>
Machine actionable</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
Data in a format that can be automatically read and processed by a computer, such as CSV, JSON, XML, etc. is considered Machine Readable. Machine-readable data must be structured data.”, [Open Data Handbook](https://opendatahandbook.org/glossary/en/terms/machine-readable/)</li>
<li><strong>Agricultural Example:</strong>
Dates during which crops were planted, entered into a spreadsheet in a standard format, would be considered machine-readable data. Were the data instead to be kept in a notebook or table in a PDF, it would not necessarily be machine-readable, as a computer would struggle to access the information, despite its readability to humans. Reference:</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="metadata"></a>
<div class="icon-container"><h3><img src="../img/Metadata.png" alt="Metadata icon" class="icon-image" width="50" height="50">&nbsp;<term>
Metadata</term></h3></div>
<id="definition"><i>
Information about a piece of data or dataset, including title, description, area, and time period covered. It is essentially “data about data” and aids in the findability and usability of the data. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Data<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
Metadata could provide information about field data, including when and where data was captured or information about the equipment or methods used to collect the samples.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>

## N {#n}

<a name="noncommercial_data_use"></a>
<div class="icon-container"><h3><img src="../img/Non-commercial Data Use.png" alt="Non-commercial Data Use icon" class="icon-image" width="50" height="50">&nbsp;<term>
Non-commercial Data Use</term></h3></div>
<id="definition"><i>
The data cannot be used for commercial purposes (i.e., not intended to make a profit)</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Data use<br>
<strong>Related Terms:</strong>
commercial Data Use</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
Non-commercial uses of data include government activities, research (academic or government), philanthropy and advocacy, individual interest, etc.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>

## O {#o}

<a name="open_data"></a>
<div class="icon-container"><h3><img src="../img/Open Data.png" alt="Open Data icon" class="icon-image" width="50" height="50">&nbsp;<term>
Open Data</term></h3></div>
<id="definition"><i>
Data that is available to be freely accessed, used, modified, and shared for any purpose, often only subject to a requirement to attribute the data’s source. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Data types<br>
<strong>Related Terms:</strong>
Open Source</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
"Open data initiatives seek to increase government transparency and accountability by proactively releasing data sets and making them freely available to anyone for use and republishing. Given the increased amount and availability of information these initiatives provide, it is important that institutions release their data sets in a way that protects the privacy of individuals. Open data initiatives also seek to promote research, innovation and the development of new applications and services. The greater the utility of open data sets, the better the chances of success for researchers, start-up companies, and entrepreneurs seeking to use public data." (IPC Ontario 2016)</li>
<li><strong>Agricultural Example:</strong>
Open data may be a data set that can be freely shared and used to make informed decisions about farming practices. This could include resources for weather data, soil moisture data, and crop yield data that is freely available to anyone interested in accessing it.</li>
<li><strong>Reference:</strong>
[The World Bank: Open Data Essentials](http://opendatatoolkit.worldbank.org/en/essentials.html), [Open Definition](http://opendefinition.org/)</li>
<li><strong>Steward:</strong>
Berlin Declaration on Open Access to Knowledge in the Sciences and Humanities</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="open_source"></a>
<div class="icon-container"><h3><img src="../img/Open Source.png" alt="Open Source icon" class="icon-image" width="50" height="50">&nbsp;<term>
Open Source</term></h3></div>
<id="definition"><i>
A publicly accessible software or hardware tool that can be modified and shared by all of its users. This allows the tool to be designed, inspected, and enhanced by multiple contributors. Open source tools allow for more control, increased security and stability, additional training opportunities, and the foundation of communities centered around collaborative design.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
<br>
<strong>Related Terms:</strong>
Open Data</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
an agricultural software platform that can be modified or built upon to suit the specific needs of a farming operation.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>

## P {#p}

<a name="personal_data"></a>
<div class="icon-container"><h3><img src="../img/Personal data.png" alt="" class="icon-image" width="50" height="50">&nbsp;<term>
Personal data</term></h3></div>
<id="definition"><i>
Any information that directly identifies an individual or that could be used to identify the individual, either alone or in conjunction with other information (e.g., name, location, or factors specific to the person's physical, genetic, economic, cultural identity). </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Data<br>
<strong>Related Terms:</strong>
Anonymization; De-identification; Data Subject; GDPR; CCPA</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>

- 1) The California Consumer Privacy Act (CCPA) definition: “‘Personal information’ means information that identifies, relates to, describes, is reasonably capable of being associated with, or could reasonably be linked, directly or indirectly, with a particular consumer or household. Personal information includes, but is not limited to, the following if it identifies, relates to, describes, is reasonably capable of being associated with, or could be reasonably linked, directly or indirectly, with a particular consumer or household” [1798.149. Definitions (v) (1)](https://leginfo.legislature.ca.gov/faces/codes_displayText.xhtml?division=3.&part=4.&lawCode=CIV&title=1.81.5) 
- 2) The EU’s General Data Protection Regulation (GDPR) definition: “‘personal data’ means any information relating to an identified or identifiable natural person (‘data subject’); an identifiable natural person is one who can be identified, directly or indirectly, in particular by reference to an identifier such as a name, an identification number, location data, an online identifier or to one or more factors specific to the physical, physiological, genetic, mental, economic, cultural or social identity of that natural person”

</li>
<li><strong>Agricultural Example:</strong>
Personal data may be information related to the farmer’s identity, such as name, contact information, social security number, or financial information related to their farming operation. Sometimes, agricultural data could also be personal data when it makes a person identifiable.</li>
<li><strong>Reference:</strong>
CCPA, GDPR</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="privacy_by_design"></a>
<div class="icon-container"><h3><img src="../img/Privacy by Design.png" alt="Privacy by Design icon" class="icon-image" width="50" height="50">&nbsp;<term>
Privacy by Design</term></h3></div>
<id="definition"><i>
The concept that data is best protected when data privacy is integrated into a technology’s design. </i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
<br>
<strong>Related Terms:</strong>
Data Privacy; Data Management; Data Governance</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
“The term “Privacy by Design” references “data protection through technology design.” Behind this is the thought that data protection in data processing procedures is best adhered to when it is already integrated in the technology when created.” [Privacy by Design - General Data Protection Regulation (GDPR)](https://gdpr-info.eu/issues/privacy-by-design/)</li>
<li><strong>Agricultural Example:</strong>
The development of a mobile app for farmers that collects and processes personal data could utilize privacy by design. App developers should design the app in a way that protects the privacy of farmers, such as using encryption and implementing features that allow farmers to delete their data from the platform or control who has access to it.</li>
<li><strong>Reference:</strong>
[Privacy by Design - General Data Protection Regulation (GDPR)](https://gdpr-info.eu/issues/privacy-by-design/</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="proxy"></a>
<div class="icon-container"><h3><img src="../img/Proxy.png" alt="Proxy icon" class="icon-image" width="50" height="50">&nbsp;<term>
Proxy</term></h3></div>
<id="definition"><i>
An individual or group authorized and given rights by the data originator or data controller to act on their behalf with regards to their data, such as collection, processing, and sharing. Proxy rights are granted via a Proxy Agreement</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Roles<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
A farmer (who is the data originator) may authorize an agricultural advisor or consultant to act as their proxy with respect to their farming data. This person or party is then responsible for ensuring the farmer’s data is collected and processed in accordance with the farmer’s preferences and in their best interests.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="purpose_limitation"></a>
<div class="icon-container"><h3><img src="../img/Purpose Limitation.png" alt="Purpose Limitation icon" class="icon-image" width="50" height="50">&nbsp;<term>
Purpose Limitation</term></h3></div>
<id="definition"><i>
The Principle that personal data is only collected for specific and legitimate purposes. This purpose is made explicit and data is not used in a manner beyond the original purpose.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
GDPR<br>
<strong>Related Terms:</strong>
Storage Limitation; Data Minimization; Data Management; Data Governance; Data Privacy</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
“Personal data shall be… collected for specified, explicit and legitimate purposes and not further processed in a manner that is incompatible with those purposes; further processing for archiving purposes in the public interest, scientific or historical research purposes or statistical purposes shall, in accordance with Article 89(1), not be considered to be incompatible with the initial purposes (‘purpose limitation’);” [Art. 5 GDPR: Principles relating to processing of personal data](https://gdpr-info.eu/art-5-gdpr/)</li>
<li><strong>Agricultural Example:</strong>
The collection of data from sensors installed on a farm for optimizing irrigation. Purpose limitation would dictate that this data not be used for marketing or any other purpose without explicit consent from the land stewards, ensuring that their data is only used for the specific purposes they agreed to.</li>
<li><strong>Reference:</strong>
GDPR: Data protection principles</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>

## R {#r}

<a name="reusable_fair_principles"></a>
<div class="icon-container"><h3><img src="../img/Reusable (FAIR Principles).png" alt="Reusable (FAIR Principles) icon" class="icon-image" width="50" height="50">&nbsp;<term>
Reusable (FAIR Principles)</term></h3></div>
<id="definition"><i>
Data and metadata should be optimized and formatted to be easily shared, combined with other data, or used for new purposes. It should be easily available with clear usage licenses and rights. It is a key component of the FAIR Data Principles.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
FAIR Principles<br>
<strong>Related Terms:</strong>
FAIR Principles, Finable, Accessible, Interoperable</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
(1) The FAIR Principles definition: “The ultimate goal of FAIR is to optimise the reuse of data. To achieve this, metadata and data should be well-described so that they can be replicated and/or combined in different settings.” (https://www.go-fair.org/fair-principles/ )
</li>
<li><strong>Agricultural Example:</strong>
A database of crop yield information that has been inputted in a standardized format, with clear metadata and documentation, allows it to be easily reusable. This could help other farmers, researchers, or policymakers use the dataset for a variety of purposes, potentially leading to greater collaboration and innovation.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
[FAIR Principles](https://www.go-fair.org/fair-principles/)</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>

## S {#s}

<a name="service_providers"></a>
<div class="icon-container"><h3><img src="../img/Service Providers.png" alt="Service Providers icon" class="icon-image" width="50" height="50">&nbsp;<term>
Service Providers</term></h3></div>
<id="definition"><i>
A company or organization that offers services related to data management, storage, processing, analysis, or other related activities.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
Roles<br>
<strong>Related Terms:</strong>
</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
A service provider could be a company that offers technology, such as soil moisture sensors or operational management software, to help farmers collect and analyze data to optimize resource use.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>
<a name="storage_limitation"></a>
<div class="icon-container"><h3><img src="../img/Storage Limitation.png" alt="Storage Limitation icon" class="icon-image" width="50" height="50">&nbsp;<term>
Storage Limitation</term></h3></div>
<id="definition"><i>
The principle that personal data is only held for as long as the original purpose for collecting that data is unfulfilled. It should not be held after it has served its purpose.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
GDPR: Data protection principles<br>
<strong>Related Terms:</strong>
Purpose Limitation; Data Minimization; Data Management; Data Governance; Data Privacy</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
A software platform only holds onto personal data, such as location or farmer name, as long as is necessary to achieve its purpose, such as generating suggestions for optimizing irrigation to minimize water use. Once the purpose is fulfilled, the software deletes or anonymizes the data to ensure they are not storing personal data longer than necessary, thus protecting the privacy of farms and reducing the risk of data breaches.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>

## T {#t}

<a name="technical_assistance_provider_tap"></a>
<div class="icon-container"><h3><img src="../img/Technical Assistance Provider (TAP).png" alt="Technical Assistance Provider (TAP) icon" class="icon-image" width="50" height="50">&nbsp;<term>
Technical Assistance Provider (TAP)</term></h3></div>
<id="definition"><i>
An individual or organization (including government agencies, non-profit organizations, or private companies) that provides support to farmers, ranchers, and land stewards. They may provide expertise in areas such as crop or livestock management, soil health, irrigation systems, pest management, and other agricultural practices. They may also provide assistance with technology adoption, data management, and other aspects of agricultural technology.</i></id><br>
<p style="margin-left: 15px;">
<strong>Tags:</strong>
<br>
<strong>Related Terms:</strong>
Agricultural Advisor; Service Provider</p>
<details><summary><i>More details</i></summary>
<ul>
<li> <strong>Alternative Definitions: </strong>
</li>
<li><strong>Agricultural Example:</strong>
A Technical Assistance Provider (TAP) may be a specialist from an extension office who works directly with farmers to implement sustainable farming practices, such as reducing water usage by offering resources, advice, and technical support.</li>
<li><strong>Reference:</strong>
</li>
<li><strong>Steward:</strong>
</li>
<li><strong>Last Updated:</strong>
</li>
</ul>
</details>





<br>
<hr/>
<i>Disclaimer: Varied Definitions and Community-Driven Content Definitions can vary depending on individual perspectives, cultural backgrounds, legal contexts, and contextual factors. Furthermore, the definitions provided are shaped by the contributions and insights of a diverse community, which adds richness and depth to the content.
By using this resource, you acknowledge the inherent variability of definitions and recognize the community-driven nature of the content presented.</i>
