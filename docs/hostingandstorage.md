<img src="../assets/hostingv1.png" alt="Hosting and Storage Icon" class="imgcenter" width="100" height="100" >

This document is intended to provide a boilerplate agreement between [land stewards](/TermsandDefinitions#land_stewards) and/or trusted advisers and the data [service providers](/TermsandDefinitions#service_providers). Land stewards and/or their trusted advisors act as [data controllers](/TermsandDefinitions#data_controller), [originators](/TermsandDefinitions#data_originator), [stewards](/TermsandDefinitions#data_steward) or their [proxies](/TermsandDefinitions#proxy), while data [service providers](/TermsandDefinitions#service_providers) are those that host or store [agricultural data](/TermsandDefinitions#agricultural_data).  Service providers are the signatories to this agreement and are the party referred to as “we, us, or our” throughout the document. [land stewards](/TermsandDefinitions#land_stewards) and/or trusted advisers are referred to as “users” throughout the document.

# Preamble
[Service Providers](/TermsandDefinitions#service_providers) are required under this agreement to achieve and maintain a standard for hosting, storing, and securing sensitive data and agricultural information. As is consistent across all of OpenTEAM’s data use agreements, data hosts and storage providers in our ecosystem must agree to ensure that [land stewards](/TermsandDefinitions#land_stewards) and their [proxies](/TermsandDefinitions#proxy) are assured that [service providers](/TermsandDefinitions#service_providers) are upholding their data rights.

OpenTEAM’s Data Hosting and Storage Agreement follows the 8 primary principles of OpenTEAM’s Agriculturalists’ Bill of Data Rights:


1. [Ownership](/TermsandDefinitions#data_ownership) and [Sovereignty](/TermsandDefinitions#data_sovereignty)*
2. [Privacy](/TermsandDefinitions#data_privacy) and Security
3. Access and Use
4. [Portability](/TermsandDefinitions#data_portability)
5. Erasure
6. [Transparency](/TermsandDefinitions#data_transparency) and Informed [Consent](/TermsandDefinitions#consent)
7. Benefit*
8. Correction


_*Note: Principles of Ownership and Sovereignty, and Benefit are covered within conditional data use agreements._

This document establishes a framework for appropriate measures for hosting and storing confidential data. [Service Providers](/TermsandDefinitions#service_providers) agree to the terms of this document, which is intended to be paired with relevant conditional data use and proxy agreements to allow users to manage and assign ownership, sovereignty and benefits.

# I. Control & Use
**Rights Maintained: Ownership & Sovereignty; Transparency & Informed [Consent](/TermsandDefinitions#consent)**
## Identity
While using the data hosting service, a user will be required to provide verifiable unique identifiers such as an address, mobile phone number, or verified email address. These identifiers will enable a user’s secure retrieval and future portability and transfer of data rights, through [conditional use](/TermsandDefinitions#conditional_use) and [proxy](/TermsandDefinitions#proxy) agreements.

## Usage Data
Usage Data is collected automatically when using the Service only for monitoring, maintaining, and optimizing the service and providing metrics back to the users.  This monitoring is required for invoicing for [data storage](/TermsandDefinitions#data_storage) and computational cycles.

Usage Data may include information such as a user’s device's Internet Protocol address (e.g. IP address), browser type, browser version, the pages of the Service that a user visits, the time and date of a visit, the time spent on those pages, unique device identifiers and other diagnostic data.

When the user accesses the Service by or through a mobile device, the [Service Provider](/TermsandDefinitions#service_providers) may automatically collect information for ID verification, including, but not limited to, the type of mobile device used to access the service, a mobile device’s unique ID, the IP address of a mobile device, a mobile operating system, the type of mobile Internet browser used, unique device identifiers, and other diagnostic data.

## [Data Sharing](/TermsandDefinitions#data_sharing)
We will not share a user’s data except for the purposes outlined in conditional data use agreements or as required by law.  

## II. Processing and Security

**Rights Maintained: Transparency & Informed Consent; Erasure; Portability; Correction**

Best management practices for hardware and software security are implemented as outlined in current standard operating procedures and/or best management practices document which is available upon request. These conditions have been audited by [ third party certification ]. While reasonable steps will be taken to ensure data security, no method of transmission over the internet or method of electronic storage is 100% secure.

## [Data Processing](/TermsandDefinitions#data_processing)
We only have access to data through the terms set out under this agreement.

- To manage user requests: To attend and manage user requests to us.
- For business transfers: wer may use a user's information to evaluate or conduct a merger, divestiture, restructuring, reorganization, dissolution, or other sale or transfer of some or all of our assets,
- To provide and maintain the service, including to monitor the usage of the Service.
- To manage a user’s account: to manage a user's registration as a user of the Service. The Data a user provides can give the user access to different functionalities of the Service that are available to a registered user.

## Data Retention
The [Service Provider](/TermsandDefinitions#service_providers) will retain data only for as long as is necessary for the purposes set out in this Use Agreement. The [Service Provider](/TermsandDefinitions#service_providers) will retain and use data to the extent necessary to comply with its legal obligations (for example, if the [Service Provider](/TermsandDefinitions#service_providers) is required to retain data to comply with applicable laws), resolve disputes, and enforce its legal agreements and policies.

The [Service Provider](/TermsandDefinitions#service_providers) will also retain Usage Data for internal analysis purposes. Usage Data is generally retained for a shorter period of time, except when this data is used to strengthen the security or to improve the functionality of the Service, or the [Service Provider](/TermsandDefinitions#service_providers) is legally obligated to retain this data for longer time periods.

All user data are either mirrored or backed up on a specified frequency and are therefore recoverable and portable for an agreed-upon term of not less than five years, unless data has been transferred out of the system and the user has opted for erasure.

## Data Transfer
The user's information, including data, is processed at the [Service Provider](/TermsandDefinitions#service_providers)'s hosting locations and in any other places where the parties involved in the processing are located. This means that this information may be transferred to — and maintained on — computers outside the user's state, province, country, or other governmental jurisdiction where the data protection laws may differ from the user's.

The [Service Provider](/TermsandDefinitions#service_providers) will take all reasonable steps to ensure that data is treated securely and in accordance with this Agreement. No transfer of data will take place to an organization or a country unless there are adequate controls in place including the security of data and other information.


# III. Disclosure

**Rights Maintained: Transparency & Informed Consent**

## Business Transactions
If the [Service Provider](/TermsandDefinitions#service_providers) is involved in a merger, acquisition, or asset sale, data may be transferred. Data cannot be transferred in a business transaction unless the transferee accepts the obligations defined in this Agreement. The [Service Provider](/TermsandDefinitions#service_providers) will provide at least 60 days advance written notice and the opportunity to opt-in or opt-out before data is transferred and the Agreement is assumed by a new entity.

## Third-Party Services
The [Service Provider](/TermsandDefinitions#service_providers) may use third-party [service providers](/TermsandDefinitions#service_providers) to monitor, analyze, or interoperate with the service. The [Service Provider](/TermsandDefinitions#service_providers) requires all third-party [service providers](/TermsandDefinitions#service_providers) to follow the same levels of protection defined under this agreement. 

## Payments
The [Service Provider](/TermsandDefinitions#service_providers) may use third-party services for payment processing (e.g. payment processors).

The [Service Provider](/TermsandDefinitions#service_providers) will not store or collect the user's payment card details. That information is provided directly to its third-party payment processors whose use of the user's information is governed by their use agreement. All payment processors must adhere to the standards set by PCI-DSS as managed by the PCI Security Standards Council, which is a joint effort of brands like Visa, Mastercard, American Express, and Discover. PCI-DSS requirements help ensure the secure handling of payment information.

# IV. Exercising of Data Protection Rights

**Rights Maintained: Erasure, Access and Use, Correction**
The user may exercise the user's rights of access, use, erasure, and correction by contacting the [Service Provider](/TermsandDefinitions#service_providers). Please note that the [Service Provider](/TermsandDefinitions#service_providers) must ask the user to verify the user's identity before responding to such requests. If the user makes a request, the [Service Provider](/TermsandDefinitions#service_providers) will follow a best efforts process to assure timely responses [specify a defined response time here]

# V. Law enforcement and Legal Requirements

**Rights Maintained: Privacy & Security; Transparency & Informed [Consent](/TermsandDefinitions#consent)**

Under certain circumstances, the [Service Provider](/TermsandDefinitions#service_providers) may be required to disclose data by law or in response to valid requests by public authorities (e.g. a court or a government agency).

## Other Legal Requirements
The [Service Provider](/TermsandDefinitions#service_providers) may disclose data in good faith if such action is necessary to:

- Comply with a legal obligation
- Prevent or investigate possible wrongdoing in connection with the Service
- Protect the safety of users of the Service or the public
- Protect against legal liability

# Changes to this Boilerplate Agreement
This boilerplate which is intended to be customized by the parties to the agreement is governed through an annual versioning process - which can be tracked here.  The [Service Provider](/TermsandDefinitions#service_providers) is a signatory to this process - so any changes to its version and signature can be monitored through the following site.  Changes to this boilerplate agreement are effective when posted on this page. 

# Contact Us
If the user has any questions about this agreement, the user can contact us:

By visiting this page on the Service Provider site: [The service provider SITE_CONTACT_PAGE_URL]
By sending us an email: [The service provider SITE_CONTACT_EMAIL]
