

# Agricultural Data Use Documents

As OpenTEAM builds towards an equitable, [open source](/TermsandDefinitions#open_source), and interoperable technology ecosystem, it is crucial to create an approachable and accessible [consent](/TermsandDefinitions#consent) management process. The ultimate goal of this work is to have in-app, icon-based, and modular conditional data use agreements. This will enable producers (or designated trusted advisors) to determine where their personal and [agricultural data](/TermsandDefinitions#agricultural_data) is stored, how it is processed, who has access to or control over their data, and for what purposes data is used within the suite of interoperable tech tools. Our intention with this is to keep producers in control of their data, and to create [consent](/TermsandDefinitions#consent)-based structures that are human-readable. 

This site contains key documents to achieve this goal and serve as the shared foundation for the envisioned [consent](/TermsandDefinitions#consent) management structure.

# The Documents

<div class="parent">

<div class="div1"> <p class="gridtitle"> Ag Data Glossary </p>
<a href="https://openteam-agreements.community/TermsandDefinitions/"> <img src="assets/glossary.png" alt="Oath of Care" class="imgcenter" > </a>
<p class="gridp"> Establishes a foundation for a shared language and understanding of agricultural data use concepts. Each definition is linked to an icon and additional context. This is a public and searchable resource with definitions of common terms and acronyms for agricultural data use concepts.</p> </div>

<div class="div2"> <p class="gridtitle"> Data Fiduciary Oath of Care for Agricultural Professionals  </p>
<a href= "https://openteam-agreements.community/oathofcare/"> <img src="assets/oathofcare.png" alt="Oath of Care" class="imgcenter" > </a> <p class="gridp"> Serves as a trust-building document between individuals in advisory roles with producers. It is meant to establish adherence to other OpenTEAM ag data artifacts, as well as good faith activity when it comes to advising and data collection practices.</p> </div> 


<div class="div3">
<p class="gridtitle"> Agriculturalists’ Bill of Data Rights</p>
<a href="https://openteam-agreements.community/billofrights/"> <img src="assets/billofrights.png" alt="Oath of Care" class="imgcenter"> </a> <p class="gridp"> A set of rights that are ensured to producers in regard to their data within the OpenTEAM technology ecosystem. It is the responsibility of community members within the ecosystem to uphold these rights on behalf of producers, as well as to implement mechanisms for producer control. This document shapes all other OpenTEAM data artifacts, which are meant to maintain producers’ data rights.</p> </div>

<div class="div4">
<p class="gridtitle"> Data Hosting and Storage Agreement</p>
<a href="https://openteam-agreements.community/hostingandstorage/"> <img src="assets/hosting.png" alt="Oath of Care" class="imgcenter"> </a> <p class="gridp"> Defines the standard for hosting, storing, and securing producer data as a means of upholding the data privacy and security standard expected by the OpenTEAM community. This will maintain many of the rights defined in the Agriculturalists’ Bill of Data Rights, such as the right to privacy and security, erasure, access and use, portability, etc. We expect all partners who host or store agricultural data to adopt this agreement, and to provide feedback within the versioning process.
</p> </div>
</div> 

# Contributors
These documents were created, and continue to be iterated on, by OpenTEAM’s tech working group and tech community. Special thanks to contributors from Digital Green, IC-Foods, FarmOS, EPIC, Millpont, Terra Genesis, We Are For The Land Foundation, OpenTEAM Fellows, Soil & Water Outcomes Fund, Cool Farm Alliance, LandPKS, Open Rivers, LiteFarm, Open Food Network, Field to Market, Tech Matters, CFA, PASA, UBC, Point Blue, USDA-ARS, FarmOS, Terran Collective, ESMC, Our Sci, ReGrow, Element 84, & Individuals 




